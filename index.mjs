import ChartGenerator from './lib/ChartGenerator';
import fs from 'fs';
import D3Node from 'd3-node';
import d3 from 'd3';

const tsvString = fs.readFileSync('data/data.tsv', 'UTF-8').toString();
const styles = fs.readFileSync('style.css', 'UTF-8').toString();

const options = {
  styles: styles,
  d3Module: d3
}

let d3node = new D3Node(options);
let chartGenerator = new ChartGenerator(d3node, d3);
let data = d3.tsvParse(tsvString);
let name = 'mouseover';

chartGenerator.generate(name, data);


