import fs from 'fs';
import svg2png from 'svg2png';

export default class D3Output {

    static save(name, d3n) {

        fs.writeFile('dist/' + name + '.html', d3n.html(), () => {
            console.log('>> Done. Open "dist/' + name + '.html" in a web browser');
        });

        var svgBuffer = new Buffer(d3n.svgString(), 'utf-8');
        svg2png(svgBuffer)
            .then(buffer => fs.writeFile('dist/' + name + '.png', buffer))
            .catch(e => console.error('ERR:', e))
            .then(err => console.log('>> Exported: "dist/' + name + '.png"'));
    };
}
