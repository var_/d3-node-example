import chart from './chart';
import D3Output from './D3Output';

export default class ChartGenerator {

    constructor(d3n, d3) {
        this.d3node = d3n;
        this.d3 = d3;
    }

    generate(name, data) {
        let margin = {top: 20, right: 50, bottom: 30, left: 50},
            width = 960 - margin.left - margin.right,
            height = 1000 - margin.top - margin.bottom
        let svg = this.d3node.createSVG(width + margin.left + margin.right, height + margin.top + margin.bottom);

        chart(this.d3, data, svg);
        chart(this.d3, data, svg);

        // create output files
        D3Output.save(name, this.d3node)
    }
}


